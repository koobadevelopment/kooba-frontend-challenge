Kooba Frontend Challenge
===

You've made to the Kooba technical challenge... Nice one!

# Time limit
Please spend no more than 3-4 hours at a maximum on this task. If you don't get it finished within that time it's not a problem.

# Before you start
* Read through the brief below in detail and download the Sketch file below to check out the design. 
* Estimate how much time would be needed to complete the challenge. As mentioned above, if you don't think it's achievable within 3-4 hours above, that's fine.
* Make some notes about your planned approach to the task.
* Share your estimation and planned approach with me by email (ronan.mccormack@kooba.ie) before you start coding.

# The brief

Using HTML, CSS (SCSS preferably) and Javascript, we would like you to recreate the following design, including a slider (carousel) element: https://drive.google.com/file/d/1TyEe1LjMjv_fp6m3j7d9MncZRaaJbgln/view?usp=sharing

* The content should always fill the entire viewport. If the viewport size is too small to show everything, the page should scroll.
* The slider should automatically progress to the next slide every 5 seconds. No manual slider control is needed.
* No mobile design is provided, but please try to ensure things display well on mobile (you can retain or drop the slider functionality for small screens - it's up to you)
* As the slider progresses, the first slide disappears to the left and another slide enters from the right (you can repeat the three slides that are in the design).

# Some tips

* We're looking for solid use of code style best practices
* Use any packages or libraries you like for slider functionality or write the code yourself. Whatever you choose, make sure it's something you are comfortable with.
* Whatever you build should work properly in the latest versions of Chrome, Firefox and Safari. IE is not important here (Hooray!)
* Creativity - We would love to see your creativity shine: animations, micro-interactions, general inventiveness. Go wild!
* Keep performance and accessibility in mind.
* We would love to see Javascript used (no jQuery please), with ES6+ where appropriate.
* Please avoid using any styling frameworks (Bootstrap, Tailwind etc.) 

When you're ready to deliver, please send your zipped source and compiled files to ronan.mccormack@kooba.ie, or alternatively push to a git repository you can share.

If you have any questions about this assignment, feel free to get in touch with me: ronan.mccormack@kooba.ie

---

---

# Kooba Frontend base project #
You can use our base project by cloning this repo, although if you'd rather not, that's fine too.

## Gulp 4 + Webpack 4 + Babel + BrowserSync ##

For Live reloading, Browsersync has been used. For ES6 Transpilation, Babel has been used. For SourceMaps, Gulp & Webpack both have been used. Sass/CSS Workflow.

## Setup

- Install [Node](https://nodejs.org/)
- Use *Npm* that comes with Node pre-installed
- Install Gulp globally through `npm install -g gulp@next`
- Install Webpack globally through `npm install -g webpack`
- Clone this project
- `cd` to the cloned project
- Update the remote git repository to the relevant project eg. `git remote set-url origin https://koobafe@bitbucket.org/koobafe/climate-focus.git`
- Install all [packages](./package.json) with `npm install`

## Usage

- **Build the Project and Serve locally (for Development)** - `npm run dev` or `yarn run dev`. The Development port is `3000`. Build directory is `/dist`
- **Build the Project and Serve locally (for Production)** - `npm start` or `yarn start`. The Production port is `8000`. Build directory is `/build`
- **Build the Project Only (for Production)** - `npm run build` or `yarn run build`. Build directory is `/build`
- **Exporting the Project to zip file** - `npm run export` or `yarn run export`

Important Note: **Don't** run these npm scripts simultaneously.

## Appendix

- **Tooling** - Gulpfile Lives in `gulpfile.js` and Webpack config files live within `webpack` folder.
- **Source Files** - Lives in `website/src` folder
- **Compiled Files for development** - Generated into `website/dist` folder.
- **Compiled Files for production and sharing with backend** - Generated into `public/build` folder.
- **Exported Project** - The exported project is imported from `website` folder and gets exported as `website.zip` to project root
