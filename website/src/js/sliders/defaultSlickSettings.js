/**
 * Default configuration for slick sliders
 **/
export default {
    dots: false,
    speed: 600,
    autoplaySpeed: 4000,
    autoplay: true,
    variableWidth: true,
    infinite: true,
    prevArrow: "<button type='button' class='slick-prev pull-left'><i class='icon-left-arrow' aria-hidden='true'></i></button>",
    nextArrow: "<button type='button' class='slick-next pull-right'><i class='icon-right-arrow' aria-hidden='true'></i></button>",
    mobileFirst: true,
    centerMode: true,
    arrows: true
};
