import closest from './closest';
import svgClassList from './svgClassList';
import fetch from './fetch';

export default () => {
    closest();
    svgClassList();
    fetch();
};
